
let quoteBank = [
'The Way Get Started Is To Quit Talking And Begin Doing.\-Walt Disney',
'The Pessimist Sees Difficulty In Every Opportunity. The Optimist Sees Opportunity In Every Difficulty. \-Winston Churchill',
'Don\'t Let Yesterday Take Up Too Much Of Today. \-Will Rogers',
'You Learn More From Failure Than From Success. Don\'t Let It Stop You. Failure Builds Character. \-Unknown',
'It\'s Not Whether You Get Knocked Down, It\'s Whether You Get Up. \-Vince Lombardi',
'If You Are Working On Something That You Really Care About, You Don\'t Have To Be Pushed. The Vision Pulls You. \- Steve Jobs',
'People Who Are Crazy Enough To Think They Can Change The World, Are The Ones Who Do. \-Rob Siltanen',
'We May Encounter Many Defeats But We Must Not Be Defeated. \- Maya Angelou',
'I have not failed. I\'ve just found 10,000 ways that won\'t work. \-Thomas Edison',
'Life is like riding a bicycle. To keep your balance you must keep moving. \-Albert Einstein',
'Logic will get you from A to B. Imagination will take you everywhere. \-Albert Einstein',
'When people are lame, they love to blame. \-Robert Kiyosaki',
'If you cannot do great things, do small things in a great way. \-Napoleon Hill',
'Be thankful for what you have; you\'ll end up having more. If you concentrate on what you don\'t have, you will never, ever have enough. \-Oprah',
'All that we are is the result of what we have thought \-Buddha',
'If you judge people, you have no time to love them. \-Mother Teresa',
'Wisely, and slow. They stumble that run fast. \-William Shakespeare',
'The greatest wealth is to live content with little. \-Plato',
'The future belongs to those who prepare for it today. \-Malcolm X',
'It\'s always seems impossible until it\'s done. \-Nelson Mandela'
]


let getQuote = () => {
	let x = Math.floor(Math.random() * quoteBank.length);
	document.getElementById('display').innerHTML = quoteBank[x];
}